package omr.courses.unit.domain;

import omr.courses.domain.Course;
import omr.courses.domain.CourseValidator;
import omr.courses.exception.CourseValidationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.Period;

@RunWith(SpringRunner.class)
public class CourseValidatorTest {

    private Course course;

    @InjectMocks
    private CourseValidator courseValidator;

    @Before
    public void setUp() {
        course = new Course();
        course.setStartDate(LocalDate.now());
        course.setEndDate(LocalDate.now());
        course.setDuration("3d");
    }

    @Test
    public void allGood() {
        courseValidator.validateCourse(course);
    }

    @Test(expected = CourseValidationException.class)
    public void when_startDateIsGreaterThanEndDate_expect_error() {
        course.setEndDate(LocalDate.now()
                                   .minus(Period.ofDays(2)));
        courseValidator.validateCourse(course);
    }

    @Test(expected = CourseValidationException.class)
    public void when_DurationHasNoDigit_expect_error() {
        course.setDuration("dd");
        courseValidator.validateCourse(course);
    }

    @Test(expected = CourseValidationException.class)
    public void when_DurationHasNoLetter_expect_error() {
        course.setDuration("22");
        courseValidator.validateCourse(course);
    }

    @Test(expected = CourseValidationException.class)
    public void when_DurationHasInvalidLetter_expect_error() {
        course.setDuration("2r");
        courseValidator.validateCourse(course);
    }

}
