package omr.courses.unit.service;

import omr.courses.domain.Course;
import omr.courses.exception.CourseDeletionException;
import omr.courses.repository.CourseRepository;
import omr.courses.service.CourseDeletionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CourseDeletionServiceTest {

    @MockBean
    private CourseRepository courseRepository;

    @InjectMocks
    private CourseDeletionService courseDeletionService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Course buildCourse() {
        Course course = new Course();
        course.setId(123L);
        return course;
    }

    @Test
    public void when_deleteValidCourse_expect_success() {
        Mockito.doNothing()
               .when(courseRepository)
               .deleteById(ArgumentMatchers.anyLong());
        Course course = buildCourse();
        courseDeletionService.deleteCourseById(course.getId());
    }

    @Test(expected = CourseDeletionException.class)
    public void when_deleteValidCourseFails_expect_error() {
        Mockito.doThrow(new CourseDeletionException("Error occurred"))
               .when(courseRepository)
               .deleteById(ArgumentMatchers.anyLong());
        Course course = buildCourse();
        courseDeletionService.deleteCourseById(course.getId());
    }

}
