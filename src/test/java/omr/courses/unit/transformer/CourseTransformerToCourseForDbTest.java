package omr.courses.unit.transformer;

import omr.courses.domain.Course;
import omr.courses.exception.CourseValidationException;
import omr.courses.model.CourseProvider;
import omr.courses.model.CourseType;
import omr.courses.repository.CourseProviderRepository;
import omr.courses.repository.CourseTypeRepository;
import omr.courses.transformer.CourseTransformerToCourseForDb;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.Period;

@RunWith(SpringRunner.class)
public class CourseTransformerToCourseForDbTest {

    @MockBean
    private CourseTypeRepository courseTypesRepository;

    @MockBean
    private CourseProviderRepository courseProviderRepository;

    @InjectMocks
    private CourseTransformerToCourseForDb transformer;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void when_convertingCourseFromDomainToCourseForDbAndProviderDoesAlreadyExistInDb_expect_success() {
        CourseProvider courseProvider = new CourseProvider();
        courseProvider.setId(1L);
        courseProvider.setName("QA");
        Mockito.when(courseProviderRepository.findByName(ArgumentMatchers.eq("QA")))
               .thenReturn(courseProvider);
        CourseType courseType = new CourseType();
        courseType.setId(1L);
        courseType.setName("online");
        Mockito.when(courseTypesRepository.findByName(ArgumentMatchers.eq("online")))
               .thenReturn(courseType);
        Course courseFromDomain = buildCourseFromDomain();
        omr.courses.model.Course courseForDb = transformer.transform(courseFromDomain);
        Assertions.assertThat(courseForDb)
                  .isNotNull();
        Assertions.assertThat(courseForDb.getComments())
                  .isEqualTo("Not bad");
        Assertions.assertThat(courseForDb.getProvider()
                                         .getName())
                  .isEqualTo("QA");
        Assertions.assertThat(courseForDb.getDuration())
                  .isEqualTo("3d");
        Assertions.assertThat(courseForDb.getInstructors())
                  .isEqualTo("Me and you");
        Assertions.assertThat(courseForDb.getLocation())
                  .isEqualTo("London");
        Assertions.assertThat(courseForDb.getTitle())
                  .isEqualTo("A great course about UML!!!");
        Assertions.assertThat(courseForDb.getType()
                                         .getName())
                  .isEqualTo("online");
        Assertions.assertThat(courseForDb.getStartDate())
                  .isNotNull();
        Assertions.assertThat(courseForDb.getEndDate())
                  .isNotNull();
        Assertions.assertThat(courseForDb.getStartDate())
                  .isBefore(courseForDb.getEndDate());

        Mockito.verify(courseTypesRepository, Mockito.times(1))
               .findByName(ArgumentMatchers.eq("online"));
        Mockito.verify(courseProviderRepository, Mockito.times(1))
               .findByName(ArgumentMatchers.eq("QA"));
        Mockito.verify(courseProviderRepository, Mockito.times(0))
               .save(ArgumentMatchers.any(CourseProvider.class));
    }

    @Test
    public void when_convertingCourseFromDomainToCourseForDbAndProviderDoesNotExistInDb_expect_success() {
        Mockito.when(courseProviderRepository.findByName(ArgumentMatchers.eq("QA")))
               .thenReturn(null);
        CourseType courseType = new CourseType();
        courseType.setId(1L);
        courseType.setName("online");
        Mockito.when(courseTypesRepository.findByName(ArgumentMatchers.eq("online")))
               .thenReturn(courseType);
        Course courseFromDomain = buildCourseFromDomain();
        omr.courses.model.Course courseForDb = transformer.transform(courseFromDomain);
        Assertions.assertThat(courseForDb)
                  .isNotNull();
        Mockito.verify(courseTypesRepository, Mockito.times(1))
               .findByName(ArgumentMatchers.eq("online"));
        Mockito.verify(courseProviderRepository, Mockito.times(1))
               .findByName(ArgumentMatchers.eq("QA"));
        Mockito.verify(courseProviderRepository, Mockito.times(1))
               .save(ArgumentMatchers.any(CourseProvider.class));
    }

    @Test(expected = CourseValidationException.class)
    public void when_convertingCourseFromDomainWithoutCourseTypeToCourseForDb_expect_failure() {
        Mockito.when(courseProviderRepository.findByName(ArgumentMatchers.eq("QA")))
               .thenReturn(null);
        Course courseFromDomain = buildCourseFromDomain();
        courseFromDomain.setType(null);
        transformer.transform(courseFromDomain);
        Mockito.verify(courseTypesRepository, Mockito.times(0))
               .findByName(ArgumentMatchers.eq("ONLINE"));
        Mockito.verify(courseProviderRepository, Mockito.times(0))
               .findByName(ArgumentMatchers.eq("QA"));
        Mockito.verify(courseProviderRepository, Mockito.times(0))
               .save(ArgumentMatchers.any(CourseProvider.class));
    }

    private Course buildCourseFromDomain() {
        Course course = new Course();
        course.setTitle("A great course about UML!!!");
        course.setStartDate(LocalDate.now());
        course.setEndDate(LocalDate.now()
                                   .plus(Period.ofDays(3)));
        course.setType(omr.courses.domain.CourseType.ONLINE);
        course.setDuration("3d");
        course.setProvider("QA");
        course.setComments("Not bad");
        course.setLocation("London");
        course.setInstructors("Me and you");
        return course;
    }
}
