package omr.courses.unit.transformer;

import omr.courses.domain.Course;
import omr.courses.model.CourseProvider;
import omr.courses.model.CourseType;
import omr.courses.transformer.CourseTransformerToCourseForDomain;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;

@RunWith(SpringRunner.class)
public class CourseTransformerToCourseForDomainTest {

    @InjectMocks
    private CourseTransformerToCourseForDomain courseTransformerToCourseForDomain;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void when_convertingCourseFromDbToCourseForDomain_expect_success() {
        Course courseForDomain = courseTransformerToCourseForDomain.transform(createCourseFromDb());
        Assertions.assertThat(courseForDomain)
                  .isNotNull();
        Assertions.assertThat(courseForDomain.getTitle())
                  .isEqualTo("Docker course");
        Assertions.assertThat(courseForDomain.getStartDate())
                  .isNotNull();
        Assertions.assertThat(courseForDomain.getEndDate())
                  .isNotNull();
        Assertions.assertThat(courseForDomain.getStartDate())
                  .isBeforeOrEqualTo(courseForDomain.getEndDate());
        Assertions.assertThat(courseForDomain.getType()
                                             .getName())
                  .isEqualTo("online");
        Assertions.assertThat(courseForDomain.getDuration())
                  .isEqualTo("3h");
        Assertions.assertThat(courseForDomain.getProvider())
                  .isEqualTo("QA");
        Assertions.assertThat(courseForDomain.getComments())
                  .isEqualTo("Some comments");
        Assertions.assertThat(courseForDomain.getInstructors())
                  .isEqualTo("Me and you");
        Assertions.assertThat(courseForDomain.getLocation())
                  .isEqualTo("London");
    }

    private omr.courses.model.Course createCourseFromDb() {
        omr.courses.model.Course course = new omr.courses.model.Course();
        course.setId(1L);
        course.setTitle("Docker course");
        course.setStartDate(new Date(System.currentTimeMillis()));
        course.setDuration("3h");
        course.setEndDate(new Date(System.currentTimeMillis() + 20000));
        CourseType courseType = new CourseType();
        courseType.setId(1L);
        courseType.setName("online");
        course.setType(courseType);
        CourseProvider courseProvider = new CourseProvider();
        courseProvider.setId(1L);
        courseProvider.setName("QA");
        course.setProvider(courseProvider);
        course.setComments("Some comments");
        course.setInstructors("Me and you");
        course.setLocation("London");
        return course;
    }
}
