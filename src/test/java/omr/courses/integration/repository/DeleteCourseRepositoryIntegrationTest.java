package omr.courses.integration.repository;

import omr.courses.model.Course;
import omr.courses.repository.CourseRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DeleteCourseRepositoryIntegrationTest {

    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void when_deleteExistingCourse_expect_success() {
        Optional<Course> course = courseRepository.findById(1L);
        courseRepository.delete(course.get());
        Iterable<Course> courseTypes = courseRepository.findAll();
        Assertions.assertThat(courseTypes)
                  .hasSize(2);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void when_deleteByIdThatDoesNotExist_expect_exception() {
        courseRepository.deleteById(211L);
    }
}
