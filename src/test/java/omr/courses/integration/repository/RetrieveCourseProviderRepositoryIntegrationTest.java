package omr.courses.integration.repository;

import omr.courses.model.CourseProvider;
import omr.courses.repository.CourseProviderRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RetrieveCourseProviderRepositoryIntegrationTest {

    private static final String[] PROVIDERS = new String[]{"QA", "edx", "Coursera", "Udemy"};

    @Autowired
    private CourseProviderRepository courseProviderRepository;

    @Test
    public void when_findAll_expect_success() {
        Iterable<CourseProvider> courseProviderIterable = courseProviderRepository.findAll();
        Assertions.assertThat(courseProviderIterable)
                  .hasSize(4);

        List<CourseProvider> courseProviderList = StreamSupport.stream(courseProviderIterable.spliterator(), false)
                                                               .collect(Collectors.toList());
        for (int i = 0; i < courseProviderList.size(); i++) {
            CourseProvider courseProvider = courseProviderList.get(i);
            Assertions.assertThat(courseProvider.getId())
                      .isEqualTo(i + 1);
            Assertions.assertThat(courseProvider.getName())
                      .isEqualTo(PROVIDERS[i]);
        }

        // another way of doing the same thing
        IntStream.range(0, courseProviderList.size())
                 .forEach(idx -> {
                     CourseProvider courseProvider = courseProviderList.get(idx);
                     Assertions.assertThat(courseProvider.getId())
                               .isEqualTo(idx + 1);
                     Assertions.assertThat(courseProvider.getName())
                               .isEqualTo(PROVIDERS[idx]);
                 });
    }

    @Test
    public void when_findById_expect_success() {
        Optional<CourseProvider> courseProviderOptional = courseProviderRepository.findById(2L);
        Assertions.assertThat(courseProviderOptional)
                  .isPresent();
        CourseProvider courseProvider = courseProviderOptional.get();
        Assertions.assertThat(courseProvider.getId())
                  .isEqualTo(2L);
        Assertions.assertThat(courseProvider.getName())
                  .isEqualTo("edx");
    }

    @Test
    public void when_findByIdDoesNotExist_expect_EmptyOptional() {
        Optional<CourseProvider> courseProviderOptional = courseProviderRepository.findById(211L);
        Assertions.assertThat(courseProviderOptional)
                  .isNotPresent();
    }

    @Test
    public void when_findByName_expect_success() {
        CourseProvider courseProvider = courseProviderRepository.findByName("Coursera");
        Assertions.assertThat(courseProvider)
                  .isNotNull();
        Assertions.assertThat(courseProvider.getId())
                  .isEqualTo(3L);
    }

    @Test
    public void when_findByNameDoesNotExist_expect_null() {
        CourseProvider courseProvider = courseProviderRepository.findByName("MyGreatProvider");
        Assertions.assertThat(courseProvider)
                  .isNull();
    }
}
