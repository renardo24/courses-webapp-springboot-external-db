package omr.courses.integration.repository;

import omr.courses.model.CourseProvider;
import omr.courses.repository.CourseProviderRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UpdateCourseProviderRepositoryIntegrationTest {

    @Autowired
    private CourseProviderRepository courseProviderRepository;

    @Test
    public void when_updateExistingCourseProvider_expect_success() {
        Optional<CourseProvider> courseProviderOptional = courseProviderRepository.findById(2L);
        Assertions.assertThat(courseProviderOptional)
                  .isPresent();
        CourseProvider courseProvider = courseProviderOptional.get();
        courseProvider.setName("Updated Course Provider");
        courseProviderRepository.save(courseProvider);
        CourseProvider updatedCourseProvider = courseProviderRepository.findById(2L)
                                                                       .get();
        Assertions.assertThat(courseProvider.getId())
                  .isEqualTo(2L);
        Assertions.assertThat(courseProvider.getName())
                  .isEqualTo("Updated Course Provider");
    }
}
