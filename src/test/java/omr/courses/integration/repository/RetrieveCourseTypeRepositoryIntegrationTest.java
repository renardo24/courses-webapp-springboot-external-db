package omr.courses.integration.repository;

import omr.courses.model.CourseType;
import omr.courses.repository.CourseTypeRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RetrieveCourseTypeRepositoryIntegrationTest {

    private static final String[] TYPES = new String[]{"classroom", "online", "mooc"};

    @Autowired
    private CourseTypeRepository courseTypeRepository;

    @Test
    public void when_findAll_expect_success() {
        Iterable<CourseType> courseTypeIterable = courseTypeRepository.findAll();
        Assertions.assertThat(courseTypeIterable)
                  .hasSize(3);

        List<CourseType> courseTypeList = StreamSupport.stream(courseTypeIterable.spliterator(), false)
                                                       .collect(Collectors.toList());
        for (int i = 0; i < courseTypeList.size(); i++) {
            CourseType courseType = courseTypeList.get(i);
            Assertions.assertThat(courseType.getId())
                      .isEqualTo(i + 1);
            Assertions.assertThat(courseType.getName())
                      .isEqualTo(TYPES[i]);
        }

        // another way of doing the same thing
        IntStream.range(0, courseTypeList.size())
                 .forEach(idx -> {
                     CourseType courseType = courseTypeList.get(idx);
                     Assertions.assertThat(courseType.getId())
                               .isEqualTo(idx + 1);
                     Assertions.assertThat(courseType.getName())
                               .isEqualTo(TYPES[idx]);
                 });
    }

    @Test
    public void when_findById_expect_success() {
        Optional<CourseType> courseTypeOptional = courseTypeRepository.findById(2L);
        Assertions.assertThat(courseTypeOptional)
                  .isPresent();
        CourseType courseType = courseTypeOptional.get();
        Assertions.assertThat(courseType.getId())
                  .isEqualTo(2L);
        Assertions.assertThat(courseType.getName())
                  .isEqualTo("online");
    }

    @Test
    public void when_findByIdDoesNotExist_expect_EmptyOptional() {
        Optional<CourseType> courseTypeOptional = courseTypeRepository.findById(211L);
        Assertions.assertThat(courseTypeOptional)
                  .isNotPresent();
    }

    @Test
    public void when_findByName_expect_success() {
        CourseType courseType = courseTypeRepository.findByName("classroom");
        Assertions.assertThat(courseType)
                  .isNotNull();
        Assertions.assertThat(courseType.getId())
                  .isEqualTo(1L);
    }

    @Test
    public void when_findByNameDoesNotExist_expect_null() {
        CourseType courseType = courseTypeRepository.findByName("GreatCourseType");
        Assertions.assertThat(courseType)
                  .isNull();
    }
}
