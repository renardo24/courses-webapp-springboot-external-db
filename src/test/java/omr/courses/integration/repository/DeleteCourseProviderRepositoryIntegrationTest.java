package omr.courses.integration.repository;

import omr.courses.model.CourseProvider;
import omr.courses.repository.CourseProviderRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DeleteCourseProviderRepositoryIntegrationTest {

    @Autowired
    private CourseProviderRepository courseProviderRepository;

    @Test
    public void when_deleteExistingCourseProvider_expect_success() {
        CourseProvider courseProvider = courseProviderRepository.findByName("QA");
        courseProviderRepository.delete(courseProvider);
        Iterable<CourseProvider> courseProviders = courseProviderRepository.findAll();
        Assertions.assertThat(courseProviders)
                  .hasSize(3);
    }

    @Test
    public void when_deleteById_expect_success() {
        courseProviderRepository.deleteById(2L);
        Iterable<CourseProvider> courseProviders = courseProviderRepository.findAll();
        Assertions.assertThat(courseProviders)
                  .hasSize(3);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void when_deleteByIdThatDoesNotExist_expect_exception() {
        courseProviderRepository.deleteById(211L);
    }

    @Test
    public void when_deleteAllCourseProviders_expect_success() {
        courseProviderRepository.deleteAll();
        Iterable<CourseProvider> courseProviders = courseProviderRepository.findAll();
        Assertions.assertThat(courseProviders)
                  .hasSize(0);
    }
}
