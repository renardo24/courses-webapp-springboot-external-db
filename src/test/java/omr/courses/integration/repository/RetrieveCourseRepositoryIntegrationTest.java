package omr.courses.integration.repository;

import omr.courses.model.Course;
import omr.courses.repository.CourseRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RetrieveCourseRepositoryIntegrationTest {

    private static final String[] TITLES = new String[]{"Docker - Hands-on", "Ansible", "Java 8", };

    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void when_findAll_then_OK() {
        Iterable<Course> courseIterable = courseRepository.findAll();
        Assertions.assertThat(courseIterable)
                  .hasSize(3);

        List<Course> courseList = StreamSupport.stream(courseIterable.spliterator(), false)
                                               .collect(Collectors.toList());
        for (int i = 0; i < courseList.size(); i++) {
            Course course = courseList.get(i);
            Assertions.assertThat(course.getId())
                      .isEqualTo(i + 1);
            Assertions.assertThat(course.getTitle())
                      .isEqualTo(TITLES[i]);
        }

        // another way of doing the same thing
        IntStream.range(0, courseList.size())
                 .forEach(idx -> {
                     Course course = courseList.get(idx);
                     Assertions.assertThat(course.getId())
                               .isEqualTo(idx + 1);
                     Assertions.assertThat(course.getTitle())
                               .isEqualTo(TITLES[idx]);
                 });
    }

    @Test
    public void when_findById_then_OK() {
        Optional<Course> courseOptional = courseRepository.findById(2L);
        Assertions.assertThat(courseOptional)
                  .isPresent();
        Course course = courseOptional.get();
        Assertions.assertThat(course.getId())
                  .isEqualTo(2L);
        Assertions.assertThat(course.getTitle())
                  .isEqualTo("Ansible");
    }

    @Test
    public void when_findByIdDoesNotExist_then_EmptyOptional() {
        Optional<Course> courseOptional = courseRepository.findById(211L);
        Assertions.assertThat(courseOptional)
                  .isNotPresent();
    }
}
