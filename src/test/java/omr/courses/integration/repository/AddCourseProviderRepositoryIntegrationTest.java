package omr.courses.integration.repository;

import omr.courses.model.CourseProvider;
import omr.courses.repository.CourseProviderRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AddCourseProviderRepositoryIntegrationTest {

    @Autowired
    private CourseProviderRepository courseProviderRepository;

    @Test
    public void when_addNewCourseProvider_expect_success() {
        CourseProvider courseProvider = new CourseProvider();
        courseProvider.setName("Learning Tree");
        courseProviderRepository.save(courseProvider);
        Iterable<CourseProvider> courseProviders = courseProviderRepository.findAll();
        Assertions.assertThat(courseProviders)
                  .hasSize(5);
    }
}
