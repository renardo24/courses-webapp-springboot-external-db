package omr.courses.integration.repository;

import omr.courses.model.CourseType;
import omr.courses.repository.CourseTypeRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AddCourseTypeRepositoryIntegrationTest {

    @Autowired
    private CourseTypeRepository courseTypeRepository;

    @Test
    public void when_addNewCourseType_expect_success() {
        CourseType courseType = new CourseType();
        courseType.setName("MyGreatCourseType");
        courseTypeRepository.save(courseType);
        Iterable<CourseType> courseTypes = courseTypeRepository.findAll();
        Assertions.assertThat(courseTypes)
                  .hasSize(4);
    }
}
