package omr.courses.integration.repository;

import omr.courses.model.Course;
import omr.courses.model.CourseProvider;
import omr.courses.model.CourseType;
import omr.courses.repository.CourseProviderRepository;
import omr.courses.repository.CourseRepository;
import omr.courses.repository.CourseTypeRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AddCourseRepositoryIntegrationTest {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseProviderRepository courseProviderRepository;

    @Autowired
    private CourseTypeRepository courseTypeRepository;

    @Test
    public void when_addNewCourse_expect_success() {
        Course course = new Course();
        course.setTitle("ElasticSearch 5");
        Optional<CourseProvider> courseProviderOptional = courseProviderRepository.findById(2L);
        course.setProvider(courseProviderOptional.get());
        Optional<CourseType> courseTypeOptional = courseTypeRepository.findById(2L);
        course.setType(courseTypeOptional.get());
        course.setDuration("3d");
        course.setStartDate(new Date(System.currentTimeMillis() - 5000000));
        course.setEndDate(new Date(System.currentTimeMillis()));
        course.setLocation("London");
        courseRepository.save(course);
        Iterable<Course> courses = courseRepository.findAll();
        Assertions.assertThat(courses)
                  .hasSizeGreaterThanOrEqualTo(4);
    }
}
