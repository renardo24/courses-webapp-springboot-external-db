package omr.courses.integration.repository;

import omr.courses.model.CourseType;
import omr.courses.repository.CourseTypeRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UpdateCourseTypeRepositoryIntegrationTest {

    @Autowired
    private CourseTypeRepository courseTypeRepository;

    @Test
    public void when_updateExistingCourseProvider_expect_success() {
        Optional<CourseType> courseTypeOptional = courseTypeRepository.findById(2L);
        Assertions.assertThat(courseTypeOptional)
                  .isPresent();
        CourseType courseType = courseTypeOptional.get();
        courseType.setName("OnTheMoon");
        courseTypeRepository.save(courseType);

        CourseType updatedCourseType = courseTypeRepository.findById(2L)
                                                           .get();
        Assertions.assertThat(courseType.getId())
                  .isEqualTo(2L);
        Assertions.assertThat(courseType.getName())
                  .isEqualTo("OnTheMoon");
    }
}
