package omr.courses.integration.repository;

import omr.courses.model.Course;
import omr.courses.repository.CourseProviderRepository;
import omr.courses.repository.CourseRepository;
import omr.courses.repository.CourseTypeRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UpdateCourseRepositoryIntegrationTest {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseProviderRepository courseProviderRepository;

    @Autowired
    private CourseTypeRepository courseTypeRepository;

    @Test
    public void when_addNewCourse_expect_success() {
        Optional<Course> courseOptional = courseRepository.findById(2L);
        Assertions.assertThat(courseOptional)
                  .isPresent();
        Course course = courseOptional.get();
        course.setTitle("React.js course");
        courseRepository.save(course);

        Course updatedCourse = courseRepository.findById(2L)
                                               .get();
        Assertions.assertThat(updatedCourse.getId())
                  .isEqualTo(2L);
        Assertions.assertThat(updatedCourse.getTitle())
                  .isEqualTo("React.js course");
    }
}
