package omr.courses.integration.service;

import omr.courses.domain.Course;
import omr.courses.domain.CourseType;
import omr.courses.exception.CourseNotFoundException;
import omr.courses.service.CourseRetrievalService;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseRetrievalServiceIntegrationTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private CourseRetrievalService courseRetrievalService;

    @Test
    public void when_getAllCourses_expect_success() {
        List<Course> courses = courseRetrievalService.getCourses();
        Assertions.assertThat(courses)
                  .isNotNull();
        Assertions.assertThat(courses)
                  .isNotEmpty();
        Assertions.assertThat(courses)
                  .hasSize(3);
    }

    @Test
    public void when_getCourseById_expect_success() {
        Course course = courseRetrievalService.getCourseById(3L);
        Assertions.assertThat(course)
                  .isNotNull();
        Assertions.assertThat(course.getId())
                  .isEqualTo(3L);
        Assertions.assertThat(course.getType())
                  .isEqualTo(CourseType.CLASSROOM);
        Assertions.assertThat(course.getTitle())
                  .isEqualTo("Java 8");
    }

    @Test
    public void when_getCourseByIdThatDoesNotExist_expect_exception() {
        expectedException.expect(CourseNotFoundException.class);
        expectedException.expectMessage("Course with course id 50 not found.");
        courseRetrievalService.getCourseById(50L);
    }
}
