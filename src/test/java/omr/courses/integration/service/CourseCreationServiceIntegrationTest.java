package omr.courses.integration.service;

import omr.courses.domain.Course;
import omr.courses.domain.CourseType;
import omr.courses.exception.CourseCreationException;
import omr.courses.service.CourseCreationService;
import omr.courses.service.CourseRetrievalService;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseCreationServiceIntegrationTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private CourseCreationService courseCreationService;
    @Autowired
    private CourseRetrievalService courseRetrievalService;

    @Test
    public void when_addNewCourse_expect_success() {
        Course course = new Course("Ansible 2");
        course.setType(CourseType.CLASSROOM);
        course.setLocation("London");
        course.setProvider("QA");
        course.setDuration("5d");
        course.setStartDate(LocalDate.now()
                                     .minusDays(10));
        course.setEndDate(LocalDate.now()
                                   .minusDays(5));
        courseCreationService.addCourse(course);

        List<Course> courses = courseRetrievalService.getCourses();
        Assertions.assertThat(courses)
                  .isNotNull();
        Assertions.assertThat(courses)
                  .isNotEmpty();
        Assertions.assertThat(courses)
                  .hasSize(4);
        // using isGreaterThanOrEqualTo as isEqualTo sometimes fails as the id
        // can be 5L as the in-memory DB has already assigned the id to one
        // of the failing tests below
        Assertions.assertThat(courses.get(3)
                                     .getId())
                  .isGreaterThanOrEqualTo(4L);
    }

    @Test(expected = CourseCreationException.class)
    public void when_addInvalidCourseAsEndDateIsMissing_expect_exception() {
        Course course = new Course("Node.js");
        course.setType(CourseType.ONLINE);
        course.setLocation("Edinburgh");
        course.setProvider("Udemy");
        course.setDuration("5d");
        course.setStartDate(LocalDate.now()
                                     .minusDays(15));
        // missing end date
        courseCreationService.addCourse(course);
    }

    @Test()
    public void when_addInvalidCourseAsEndDateIsBeforeStartDate_expect_exception() {
        expectedException.expect(CourseCreationException.class);
        expectedException.expectMessage("could not execute statement; SQL [n/a]; constraint [\"CONSTRAINT_63E4A: (START_DATE <= END_DATE)");

        Course course = new Course("ElasticSearch 6");
        course.setType(CourseType.ONLINE);
        course.setLocation("Bristol");
        course.setProvider("Udemy");
        course.setDuration("3d");
        course.setStartDate(LocalDate.now()
                                     .minusDays(5));
        course.setEndDate(LocalDate.now()
                                   .minusDays(15));
        courseCreationService.addCourse(course);
    }

}
