package omr.courses.integration.service;

import omr.courses.domain.Course;
import omr.courses.exception.CourseDeletionException;
import omr.courses.service.CourseDeletionService;
import omr.courses.service.CourseRetrievalService;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseDeletionServiceIntegrationTest {

    @Autowired
    private CourseDeletionService courseDeletionService;
    @Autowired
    private CourseRetrievalService courseRetrievalService;

    @Test
    public void when_deleteExistingCourse_expect_success() {
        courseDeletionService.deleteCourseById(1L);
        List<Course> courses = courseRetrievalService.getCourses();
        // hasSizeGreaterThanOrEqualTo is used instead of isEqualTo because
        // when this test is run with all the other tests, the same database
        // is used and those other test add or update courses. When this test
        // is run on its own, then the size is 2. When run with other tests it
        // could be 2 or more since there is no telling the order in which these tests run.
        Assertions.assertThat(courses)
                  .isNotNull();
        Assertions.assertThat(courses)
                  .isNotEmpty();
        Assertions.assertThat(courses)
                  .hasSizeGreaterThanOrEqualTo(2);
    }

    @Test(expected = CourseDeletionException.class)
    public void when_deleteNonExistingCourse_expect_exception() {
        courseDeletionService.deleteCourseById(211L);
    }
}
