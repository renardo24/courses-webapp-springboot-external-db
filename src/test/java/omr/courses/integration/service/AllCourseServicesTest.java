package omr.courses.integration.service;

import omr.courses.domain.Course;
import omr.courses.domain.CourseType;
import omr.courses.service.CourseCreationService;
import omr.courses.service.CourseDeletionService;
import omr.courses.service.CourseRetrievalService;
import omr.courses.service.CourseUpdateService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AllCourseServicesTest {

    @Autowired
    private CourseCreationService courseCreationService;
    @Autowired
    private CourseDeletionService courseDeletionService;
    @Autowired
    private CourseRetrievalService courseRetrievalService;
    @Autowired
    private CourseUpdateService courseUpdateService;

    @Test
    public void when_testAllCourseServices_expect_success() {

        // First ensure that we can retrieve all the courses
        List<Course> courses = courseRetrievalService.getCourses();
        Assertions.assertThat(courses)
                  .isNotNull();
        Assertions.assertThat(courses)
                  .isNotEmpty();
        int numCoursesAtStart = courses.size();
        // we should have at least 3 courses
        Assertions.assertThat(numCoursesAtStart)
                  .isGreaterThanOrEqualTo(3);

        // Add a new one
        Course newCourse = createNewCourse();
        Course addedCourse = courseCreationService.addCourse(newCourse);
        Assertions.assertThat(addedCourse)
                  .isNotNull();
        // The new course id should be 4 or greater
        Assertions.assertThat(addedCourse.getId())
                  .isGreaterThanOrEqualTo(4L);
        Assertions.assertThat(addedCourse.getTitle())
                  .isEqualTo("ElasticSearch 6");

        // check that we have indeed one more course
        courses = courseRetrievalService.getCourses();
        Assertions.assertThat(courses)
                  .isNotNull();
        Assertions.assertThat(courses)
                  .isNotEmpty();
        int numCoursesAfterAddition = courses.size();
        // we should have at least 3 courses
        Assertions.assertThat(numCoursesAtStart)
                  .isGreaterThanOrEqualTo(3);
        Assertions.assertThat(numCoursesAfterAddition)
                  .isEqualTo(numCoursesAtStart + 1);

        // Update the new course
        addedCourse.setType(CourseType.ONLINE);
        Course updatedCourse = courseUpdateService.updateCourse(addedCourse);
        Assertions.assertThat(updatedCourse.getTitle())
                  .isEqualTo("ElasticSearch 6");
        Assertions.assertThat(updatedCourse.getType())
                  .isEqualTo(CourseType.ONLINE);

        // Delete it
        courseDeletionService.deleteCourseById(updatedCourse.getId());

        // Check that we have the same number of courses as we
        // had when we started this test
        courses = courseRetrievalService.getCourses();
        Assertions.assertThat(courses)
                  .isNotNull();
        Assertions.assertThat(courses)
                  .isNotEmpty();
        int numCoursesAtEnd = courses.size();
        // we should have at least 3 courses
        Assertions.assertThat(numCoursesAtStart)
                  .isGreaterThanOrEqualTo(3);
        Assertions.assertThat(numCoursesAtStart)
                  .isEqualTo(numCoursesAtEnd);
    }

    private Course createNewCourse() {
        Course course = new Course("ElasticSearch 6");
        course.setLocation("London");
        course.setDuration("3d");
        course.setStartDate(LocalDate.now()
                                     .minusWeeks(5));
        course.setEndDate(LocalDate.now()
                                   .minusWeeks(4)
                                   .minusDays(2));
        course.setProvider("QA");
        course.setType(CourseType.CLASSROOM);
        course.setComments("Great course");
        return course;
    }
}
