package omr.courses.integration.controller.resttemplate;

import omr.courses.Application;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CourseCreationControllerRestTemplateIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void when_goToCreateCoursePage_expect_success() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createUrlWithPort("/add-course-page"), HttpMethod.GET, entity, String.class);

        Assertions.assertThat(response.getStatusCodeValue())
                  .isEqualTo(200);

        Pattern courseTypePattern = Pattern.compile("<select id=\"type\" name=\"type\">\n\\s+<option value=\"ONLINE\">ONLINE</option>");
        Pattern courseProviderPattern = Pattern.compile("<td>Provider:</td>\n\\s+<td><input type=\"text\" id=\"provider\" name=\"provider\" value=\"\"/></td>");

        String responseBody = response.getBody();
        Assertions.assertThat(responseBody)
                  .contains("<title>Add New Course</title>");
        Assertions.assertThat(responseBody)
                  .contains("<h1>Add New Course</h1>");
        Assertions.assertThat(responseBody)
                  .contains("<form action=\"/add-course\" method=\"post\">");
        Assertions.assertThat(responseBody)
                  .containsPattern(courseTypePattern);
        Assertions.assertThat(responseBody)
                  .containsPattern(courseProviderPattern);
    }

    private String createUrlWithPort(final String uri) {
        return "http://localhost:" + port + uri;
    }
}
