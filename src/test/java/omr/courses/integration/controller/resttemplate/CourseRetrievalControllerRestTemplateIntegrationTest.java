package omr.courses.integration.controller.resttemplate;

import omr.courses.Application;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CourseRetrievalControllerRestTemplateIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void when_goToCoursesListPage_expect_success() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createUrlWithPort("/courses"), HttpMethod.GET, entity, String.class);

        Assertions.assertThat(response.getStatusCodeValue())
                  .isEqualTo(200);

        String responseBoy = response.getBody();
        Assertions.assertThat(responseBoy)
                  .contains("<title>Courses</title>");
        Assertions.assertThat(responseBoy)
                  .contains("<h1>Courses List</h1>");
        Assertions.assertThat(responseBoy)
                  .contains("<td><a href=\"/courses/1\">Docker - Hands-on</a></td>");
        Assertions.assertThat(responseBoy)
                  .contains("<td><a href=\"/courses/2\">Ansible</a></td>");
        Assertions.assertThat(responseBoy)
                  .contains("<td><a href=\"/courses/3\">Java 8</a></td>");
    }

    @Test
    public void when_goToSpecificCoursePage_expect_success() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createUrlWithPort("/courses/1"), HttpMethod.GET, entity, String.class);

        Assertions.assertThat(response.getStatusCodeValue())
                  .isEqualTo(200);

        Pattern courseTypePattern = Pattern.compile("<td>Type:</td>\n\\s+<td>ONLINE</td>");
        Pattern courseProviderPattern = Pattern.compile("<td>Provider:</td>\n\\s+<td>Udemy</td>");

        String responseBody = response.getBody();
        Assertions.assertThat(responseBody)
                  .contains("<title>Course Details: Docker - Hands-on</title>");
        Assertions.assertThat(responseBody)
                  .contains("<h1>Course Details: Docker - Hands-on</h1>");
        Assertions.assertThat(responseBody)
                  .contains("<td>2019-01-10</td>");
        Assertions.assertThat(responseBody)
                  .containsPattern(courseTypePattern);
        Assertions.assertThat(responseBody)
                  .containsPattern(courseProviderPattern);
    }

    private String createUrlWithPort(final String uri) {
        return "http://localhost:" + port + uri;
    }
}
