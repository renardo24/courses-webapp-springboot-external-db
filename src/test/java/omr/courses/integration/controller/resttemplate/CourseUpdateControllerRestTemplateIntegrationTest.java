package omr.courses.integration.controller.resttemplate;

import omr.courses.Application;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CourseUpdateControllerRestTemplateIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void when_goToUpdateCoursePage_expect_success() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createUrlWithPort("/update-course-page/1"), HttpMethod.GET, entity, String.class);

        Assertions.assertThat(response.getStatusCodeValue())
                  .isEqualTo(200);

        Pattern courseTypePattern = Pattern.compile("<select id=\"type\" name=\"type\">\n\\s+<option value=\"ONLINE\" selected=\"selected\">ONLINE</option>");
        Pattern courseProviderPattern = Pattern.compile("<td>Provider:</td>\n\\s+<td><input type=\"text\" id=\"provider\" name=\"provider\" value=\"Udemy\"/></td>");

        String responseBody = response.getBody();
        Assertions.assertThat(responseBody)
                  .contains("<title>Update Existing Course</title>");
        Assertions.assertThat(responseBody)
                  .contains("<h1>Update Existing Course</h1>");
        Assertions.assertThat(responseBody)
                  .contains("<form action=\"/update-course\" method=\"post\">");
        Assertions.assertThat(responseBody)
                  .containsPattern(courseTypePattern);
        Assertions.assertThat(responseBody)
                  .containsPattern(courseProviderPattern);
    }

    private String createUrlWithPort(final String uri) {
        return "http://localhost:" + port + uri;
    }
}
