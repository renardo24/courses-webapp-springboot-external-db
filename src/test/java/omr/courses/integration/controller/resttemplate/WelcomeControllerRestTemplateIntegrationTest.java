package omr.courses.integration.controller.resttemplate;

import omr.courses.Application;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WelcomeControllerRestTemplateIntegrationTest {

    private static final Pattern LIST_COURSES_PATTERN = Pattern.compile("<li><a href=\"/courses\">List courses</a></li>");
    private static final Pattern ADD_COURSE_PATTERN = Pattern.compile("<li><a href=\"/add-course-page\">Add a course</a></li>");

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void when_goToRootPage_expect_success() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createUrlWithPort("/"), HttpMethod.GET, entity, String.class);

        Assertions.assertThat(response.getStatusCodeValue())
                  .isEqualTo(200);

        String responseBody = response.getBody();
        Assertions.assertThat(responseBody)
                  .contains("<title>Courses</title>");
        Assertions.assertThat(responseBody)
                  .contains("<h1>Courses</h1>");
        Assertions.assertThat(responseBody)
                  .containsPattern(LIST_COURSES_PATTERN);
        Assertions.assertThat(responseBody)
                  .containsPattern(ADD_COURSE_PATTERN);
    }

    @Test
    public void when_goToWelcomePage_expect_success() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(createUrlWithPort("/"), HttpMethod.GET, entity, String.class);

        Assertions.assertThat(response.getStatusCodeValue())
                  .isEqualTo(200);

        String responseBody = response.getBody();
        Assertions.assertThat(responseBody)
                  .contains("<title>Courses</title>");
        Assertions.assertThat(responseBody)
                  .contains("<h1>Courses</h1>");
        Assertions.assertThat(responseBody)
                  .containsPattern(LIST_COURSES_PATTERN);
        Assertions.assertThat(responseBody)
                  .containsPattern(ADD_COURSE_PATTERN);
    }

    private String createUrlWithPort(final String uri) {
        return "http://localhost:" + port + uri;
    }
}
