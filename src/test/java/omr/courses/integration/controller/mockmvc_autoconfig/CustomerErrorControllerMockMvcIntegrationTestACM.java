package omr.courses.integration.controller.mockmvc_autoconfig;

import omr.courses.controller.CustomErrorController;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.servlet.RequestDispatcher;

@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
public class CustomerErrorControllerMockMvcIntegrationTestACM {

    @Autowired
    private MockMvc mvc;

    @Test
    public void when_httpStatusBadRequest_then_error400Page() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/error")
                                          .requestAttr(RequestDispatcher.ERROR_STATUS_CODE, 400))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.view()
                                           .name("error/error-400"));
    }

    @Test
    public void when_httpStatusNotFound_then_error404Page() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/error")
                                          .requestAttr(RequestDispatcher.ERROR_STATUS_CODE, 404))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.view()
                                           .name("error/error-404"));
    }

    @Test
    public void when_httpStatusInternalServerError_then_error500Page() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/error")
                                          .requestAttr(RequestDispatcher.ERROR_STATUS_CODE, 500))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.view()
                                           .name("error/error-500"));
    }

    @Test
    public void when_httpStatusAnythingElse_then_defaultErrorPage() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/error")
                                          .requestAttr(RequestDispatcher.ERROR_STATUS_CODE, 405))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.view()
                                           .name("error/error"));
    }

    @Test
    public void when_httpStatusIsNull_then_defaultErrorPage() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/error"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.view()
                                           .name("error/error"));
    }

    @Test
    public void when_getErrorPath_then_return_success() {
        CustomErrorController cec = new CustomErrorController();
        Assertions.assertThat(cec.getErrorPath())
                  .isEqualTo("/error");
    }
}
