DROP TABLE IF EXISTS courses_providers;
DROP TABLE IF EXISTS courses_types;
DROP TABLE IF EXISTS courses;


CREATE TABLE courses_providers (
    id SMALLINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(500) NOT NULL,
    UNIQUE KEY courses_providers_name_unique (name)
);


CREATE TABLE courses_types (
    id SMALLINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    UNIQUE KEY courses_types_name_unique (name)
);


CREATE TABLE courses (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(3000) NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    duration VARCHAR(10) NOT NULL,
    type_id INTEGER NOT NULL,
    provider_id INTEGER NOT NULL,
    comments VARCHAR(5000) NULL,
    instructors VARCHAR(5000) NULL,
    location VARCHAR(1000) NULL,
    UNIQUE (title, start_date),
    FOREIGN KEY (type_id) REFERENCES courses_types (id),
    FOREIGN KEY (provider_id) REFERENCES courses_providers (id) ON DELETE CASCADE,
    CHECK (start_date <= end_date),
    CHECK (end_date >= start_date)
);