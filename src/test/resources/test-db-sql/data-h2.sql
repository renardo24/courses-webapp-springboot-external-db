INSERT INTO courses_types (name) VALUES ('classroom');
INSERT INTO courses_types (name) VALUES ('online');
INSERT INTO courses_types (name) VALUES ('mooc');

INSERT INTO courses_providers (name) VALUES ('QA');
INSERT INTO courses_providers (name) VALUES ('edx');
INSERT INTO courses_providers (name) VALUES ('Coursera');
INSERT INTO courses_providers (name) VALUES ('Udemy');

INSERT INTO courses (title, start_date, end_date, duration, type_id, provider_id)
    VALUES ('Docker - Hands-on', '2019-01-10', '2019-01-22', '12d',
    (SELECT id FROM courses_types WHERE name = 'online'),
    (SELECT id FROM courses_providers WHERE name = 'Udemy'));
INSERT INTO courses (title, start_date, end_date, duration, type_id, provider_id, comments)
    VALUES ('Ansible', '2018-05-08', '2018-05-10', '2d',
    (SELECT id FROM courses_types WHERE name = 'online'),
    (SELECT id FROM courses_providers WHERE name = 'Udemy'), 'Interesting but not that great');
INSERT INTO courses (title, start_date, end_date, duration, type_id, provider_id, comments, instructors, location)
    VALUES ('Java 8', '2018-06-05', '2018-06-05', '1d',
    (SELECT id FROM courses_types WHERE name = 'classroom'),
    (SELECT id FROM courses_providers WHERE name = 'QA'),
    'Interesting and informative', 'some person', 'London');