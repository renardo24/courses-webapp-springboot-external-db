package omr.courses.repository;

import omr.courses.model.CourseProvider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseProviderRepository extends CrudRepository<CourseProvider, Long> {
    CourseProvider findByName(final String name);
}
