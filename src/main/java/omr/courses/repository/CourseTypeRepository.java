package omr.courses.repository;

import omr.courses.model.CourseType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseTypeRepository extends CrudRepository<CourseType, Long> {
    CourseType findByName(final String name);
}
