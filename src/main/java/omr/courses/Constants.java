package omr.courses;

public interface Constants {
    String COURSES_LIST = "courses-list";
    String COURSE_DETAILS = "course-details";
    String COURSE_FORM_PAGE = "course-form";
}
