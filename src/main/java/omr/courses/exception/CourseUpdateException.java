package omr.courses.exception;

import omr.courses.domain.Course;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CourseUpdateException extends CourseException {

    public CourseUpdateException(final String message) {
        super(message);
    }

    public CourseUpdateException(final Course c) {
        super(c);
    }

    public CourseUpdateException(final Course c, final String message) {
        super(c, message);
    }
}
