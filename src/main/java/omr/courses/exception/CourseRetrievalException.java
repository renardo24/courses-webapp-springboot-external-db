package omr.courses.exception;

import omr.courses.domain.Course;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CourseRetrievalException extends CourseException {

    public CourseRetrievalException(final String message) {
        super(message);
    }

    public CourseRetrievalException(final Course c) {
        super(c);
    }

    public CourseRetrievalException(final Course c, final String message) {
        super(c, message);
    }
}
