package omr.courses.exception;

import omr.courses.domain.Course;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CourseCreationException extends CourseException {

    public CourseCreationException(final String message) {
        super(message);
    }

    public CourseCreationException(final Course c) {
        super(c);
    }

    public CourseCreationException(final Course c, final String message) {
        super(c, message);
    }
}
