package omr.courses.exception;

import omr.courses.domain.Course;

public class CourseException extends RuntimeException {

    private Course course;

    public CourseException() {
        super();
    }

    public CourseException(final String message) {
        super(message);
    }

    public CourseException(final Course c) {
        super();
        this.course = c;
    }

    public CourseException(final Course c, final String message) {
        super(message);
        this.course = c;
    }
}
