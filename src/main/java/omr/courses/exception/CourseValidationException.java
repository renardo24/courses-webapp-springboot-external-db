package omr.courses.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CourseValidationException extends CourseException {
    public CourseValidationException(final String message) {
        super(message);
    }
}
