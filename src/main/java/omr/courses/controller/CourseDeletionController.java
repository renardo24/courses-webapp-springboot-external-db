package omr.courses.controller;

import omr.courses.service.CourseDeletionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CourseDeletionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseDeletionController.class);

    @Autowired
    private CourseDeletionService courseDeletionService;

    @PostMapping(value = "/delete-course/{id}")
    public ModelAndView deleteCourse(@PathVariable final long id) {
        LOGGER.info("About to delete course with id: {}", id);
        courseDeletionService.deleteCourseById(id);
        LOGGER.info("Deleted course with id: {}", id);
        return new ModelAndView("redirect:/courses");
    }
}
