package omr.courses.controller;

import omr.courses.Constants;
import omr.courses.domain.Course;
import omr.courses.domain.CourseValidator;
import omr.courses.service.CourseCreationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CourseCreationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseCreationController.class);

    @Autowired
    private CourseCreationService courseCreationService;

    @Autowired
    private CourseValidator courseValidator;

    @GetMapping(value = "/add-course-page")
    @ResponseStatus(HttpStatus.OK)
    public String goToAddCoursePage(final Model model) {
        model.addAttribute("course", new Course());
        model.addAttribute("formPath", "add-course");
        model.addAttribute("pageTitle", "Add New Course");
        LOGGER.info("Retrieving page to add course");
        return Constants.COURSE_FORM_PAGE;
    }

    @PostMapping(value = "/add-course")
    public ModelAndView addCourse(@Valid @ModelAttribute("course") final Course course) {
        LOGGER.info("Adding new course");
        courseValidator.validateCourse(course);
        courseCreationService.addCourse(course);
        LOGGER.info("New course added: {}", course);
        return new ModelAndView("redirect:/courses");
    }
}
