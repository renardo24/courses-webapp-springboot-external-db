package omr.courses.domain;

public enum CourseType {
    ONLINE("online"), CLASSROOM("classroom"), MOOC("mooc");

    private String name;

    CourseType(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static CourseType fromString(final String type) {
        for (CourseType courseType : CourseType.values()) {
            if (courseType.name.equalsIgnoreCase(type)) {
                return courseType;
            }
        }
        return null;
    }
}
