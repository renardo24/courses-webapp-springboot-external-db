package omr.courses.domain;

import omr.courses.exception.CourseValidationException;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CourseValidator {

    private static final Pattern COURSE_DURATION_PATTERN = Pattern.compile("\\d{1,2}[hdwm]");

    public void validateCourse(final Course course) throws CourseValidationException {
        validateDates(course);
        validateDuration(course);
    }

    private void validateDates(final Course course) {
        if (course.getStartDate().isAfter(course.getEndDate())) {
            throw new CourseValidationException("Start date cannot be later than end date");
        }
    }

    protected void validateDuration(final Course course) throws CourseValidationException {
        String duration = course.getDuration();
        Matcher matcher = COURSE_DURATION_PATTERN.matcher(duration);
        if (! matcher.matches()) {
            throw new CourseValidationException("Duration format is invalid");
        }
    }
}
