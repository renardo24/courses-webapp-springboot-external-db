/**
 * Java classes representing the domain, i.e. the classes used by
 * the controller and service classes.
 */
package omr.courses.domain;