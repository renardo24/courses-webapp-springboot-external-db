package omr.courses.transformer;

import omr.courses.domain.Course;
import omr.courses.domain.CourseType;
import org.springframework.stereotype.Component;

@Component
public class CourseTransformerToCourseForDomain {

    public Course transform(omr.courses.model.Course courseFromDb) {
        return toCourseForDomain(courseFromDb);
    }

    protected Course toCourseForDomain(final omr.courses.model.Course courseFromDB) {
        Course courseForDomain = new Course();
        courseForDomain.setTitle(courseFromDB.getTitle());
        courseForDomain.setComments(courseFromDB.getComments());
        courseForDomain.setDuration(courseFromDB.getDuration());
        courseForDomain.setId(courseFromDB.getId());
        courseForDomain.setStartDate(courseFromDB.getStartDate()
                                                 .toLocalDate());
        courseForDomain.setEndDate(courseFromDB.getEndDate()
                                               .toLocalDate());
        courseForDomain.setInstructors(courseFromDB.getInstructors());
        courseForDomain.setLocation(courseFromDB.getLocation());
        courseForDomain.setProvider(courseFromDB.getProvider()
                                                .getName());
        courseForDomain.setType(CourseType.fromString(courseFromDB.getType()
                                                                  .getName()));
        return courseForDomain;
    }

}
