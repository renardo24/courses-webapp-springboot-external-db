package omr.courses.service;

import omr.courses.exception.CourseDeletionException;
import omr.courses.repository.CourseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class CourseDeletionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseDeletionService.class);

    @Autowired
    private CourseRepository courseRepository;

    public void deleteCourseById(final Long courseId) throws CourseDeletionException {
        try {
            courseRepository.deleteById(courseId);
        } catch (Exception e) {
            LOGGER.error(String.format("Error deleting course with id: %d", courseId), e);
            throw new CourseDeletionException(e.getMessage());
        }
    }
}
