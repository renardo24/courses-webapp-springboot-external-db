package omr.courses.service;

import omr.courses.domain.Course;
import omr.courses.exception.CourseNotFoundException;
import omr.courses.exception.CourseRetrievalException;
import omr.courses.repository.CourseRepository;
import omr.courses.transformer.CourseTransformerToCourseForDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseRetrievalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseRetrievalService.class);

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseTransformerToCourseForDomain courseTransformerToCourseForDomain;

    public List<Course> getCourses() {
        List<Course> courses = new ArrayList<>();
        Iterable<omr.courses.model.Course> coursesFromDb = courseRepository.findAll();
        for (omr.courses.model.Course courseFromDb : coursesFromDb) {
            try {
                courses.add(courseTransformerToCourseForDomain.transform(courseFromDb));
            } catch (Exception e) {
                LOGGER.error(String.format("Error transforming course: %s", courseFromDb), e);
                throw new CourseRetrievalException(e.getMessage());
            }
        }
        return courses;
    }

    public Course getCourseById(final long courseId) {
        String errorMessage = String.format("Course with course id %s not found.", courseId);
        omr.courses.model.Course course = courseRepository.findById(courseId)
                                                          .orElseThrow(() -> new CourseNotFoundException(errorMessage));
        try {
            return courseTransformerToCourseForDomain.transform(course);
        } catch (Exception e) {
            LOGGER.error(String.format("Error transforming course with id: %d", courseId), e);
            throw new CourseRetrievalException(e.getMessage());
        }
    }
}
