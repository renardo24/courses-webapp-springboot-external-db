package omr.courses.service;

import omr.courses.domain.Course;
import omr.courses.exception.CourseCreationException;
import omr.courses.repository.CourseRepository;
import omr.courses.transformer.CourseTransformerToCourseForDb;
import omr.courses.transformer.CourseTransformerToCourseForDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Transactional
@Service
public class CourseCreationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseCreationService.class);

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseTransformerToCourseForDb courseTransformerToCourseForDb;

    @Autowired
    private CourseTransformerToCourseForDomain courseTransformerToCourseForDomain;

    public Course addCourse(@Valid final Course course) throws CourseCreationException {
        try {
            omr.courses.model.Course newCourse = courseTransformerToCourseForDb.transform(course);
            newCourse = courseRepository.save(newCourse);
            LOGGER.info("addedCourse: {}", newCourse);
            return courseTransformerToCourseForDomain.transform(newCourse);
        } catch (Exception e) {
            LOGGER.error(String.format("Error adding course: %s", course), e);
            LOGGER.error("ERROR MSG:" + e.getMessage());
            throw new CourseCreationException(e.getMessage());
        }
    }
}
