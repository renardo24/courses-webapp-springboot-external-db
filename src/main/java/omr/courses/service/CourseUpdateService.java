package omr.courses.service;

import omr.courses.domain.Course;
import omr.courses.exception.CourseUpdateException;
import omr.courses.repository.CourseRepository;
import omr.courses.transformer.CourseTransformerToCourseForDb;
import omr.courses.transformer.CourseTransformerToCourseForDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Transactional
@Service
public class CourseUpdateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseUpdateService.class);

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseTransformerToCourseForDb courseTransformerToCourseForDb;

    @Autowired
    private CourseTransformerToCourseForDomain courseTransformerToCourseForDomain;

    public Course updateCourse(@Valid final Course course) throws CourseUpdateException {
        try {
            omr.courses.model.Course updatedCourse = courseTransformerToCourseForDb.transform(course);
            updatedCourse = courseRepository.save(updatedCourse);
            LOGGER.info("updatedCourse: {}", updatedCourse);
            return courseTransformerToCourseForDomain.transform(updatedCourse);
        } catch (Exception e) {
            LOGGER.error(String.format("Error updating course: %s", course), e);
            throw new CourseUpdateException(e.getMessage());
        }
    }
}
